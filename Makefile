OBJS = src/kjv.o \
       src/intset.o \
       data/data.o
CFLAGS += -Wall -Isrc/
LDLIBS += -lreadline

ssp: $(OBJS)
	$(CC) -o $@ $(LDFLAGS) $(OBJS) $(LDLIBS)

data/data.c: data/ssp.tsv data/generate.awk src/data.h
	awk -f data/generate.awk $< > $@

.PHONY: clean
clean:
	rm -rf $(OBJS) ssp
