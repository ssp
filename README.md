Beri Božjo besedo iz ukazne vrstice.
(Read the Word of God from your terminal.)

Slovenski standardni prevod.
(Slovene standard translation.)

Prirejeno po / Forked from
https://github.com/bontibon/kjv.git

## Kontakt / Contact

Izboljšave, in obvestila o napakah mi lahko pošljete po elektronski pošti, če
se le da, zakodirano z mojim PGP ključem.

Feel free to send patches and issues to me by mail, preferably encrypted with
my PGP key.

eksodus@mail2tor.com

PGP: 53BA B9D8 ECC5 54A2 B13B D069 5740 EC65 37DB 799F


## Uporaba

    uporaba: ssp [opcije] [citat...]

    Opcije:
      -A številka  število verzov konteksta po ujemajočih verzih
      -B številka  število verzov konteksta pred ujemajočimi verzi
      -C           pokaži ujemajoče verze v kontekstu odstavka
      -l           naštej knjige
      -h           prikaži pomoč

    Citat:
        <Knjiga>
            Posamezna knjiga
        <Knjiga>:<Odstavek>
            Posamezen odstavek knjige
        <Knjiga>:<Odstavek>:<Verz>[,<Verz>]...
            Posamezni verz specifičnega odstavka knjige
        <Knjiga>:<Odstavek>-<Odstavek>
            Razpon odstavkov v knjigi
        <Knjiga>:<Odstavek>:<Verz>-<Verz>
            Razpon odstavkov v odstavku knjige
        <Knjiga>:<Odstavek>:<Verz>-<Odstavek>:<Verz>
            Razpon odstavkov in verzov v knjigi

        /<Iskalni niz>
            Vsi verzi, ki se ujemajo z iskalnim nizom
        <Knjiga>/<Iskalni niz>
            Vsi verzi v knjigi, ki se ujemajo z iskalnim nizom
        <Knjiga>:<Odstavek>/<Iskalni niz>
            Vsi verzi v odstavku v knjigi, ki se ujemajo z iskalnim nizom

## Usage

    usage: ssp [flags] [reference...]

    Flags:
      -A num  number of verses of context after matching verses
      -B num  number of verses of context before matching verses
      -C      show matching verses in context of the chapter
      -l      list books
      -h      show help

    Reference:
        <Book>
            Individual book
        <Book>:<Chapter>
            Individual chapter of a book
        <Book>:<Chapter>:<Verse>[,<Verse>]...
            Individual verse(s) of a specific chapter of a book
        <Book>:<Chapter>-<Chapter>
            Range of chapters in a book
        <Book>:<Chapter>:<Verse>-<Verse>
            Range of verses in a book chapter
        <Book>:<Chapter>:<Verse>-<Chapter>:<Verse>
            Range of chapters and verses in a book

        /<Search>
            All verses that match a pattern
        <Book>/<Search>
            All verses in a book that match a pattern
        <Book>:<Chapter>/<Search>
            All verses in a chapter of a book that match a pattern

## Prevod / Build

Program `ssp` prevede z ukazom make.

The program `ssp` can be built by running make.

## Licenca / License

Javna domena / Public domain
